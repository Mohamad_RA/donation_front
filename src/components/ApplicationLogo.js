const ApplicationLogo = ({ className }) => (

    <img src="/img/logo.png" className={`${className} `} />

)

export default ApplicationLogo
